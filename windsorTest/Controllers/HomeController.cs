﻿using System.Web.Mvc;
using windsorTest.classes;

namespace windsorTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IService _service;

        public HomeController(IService _service)
        {
            this._service = _service;
        }

        public ActionResult Index()
        {
           var x = _service.GetMessage(new Details {Name = "lklklk",Description = "sadasd"});
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}