using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace windsorTest.Installers
{
    using Plumbing;

    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            RegisterMvcControllers(container);
            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(container));
            RegisterWebApiControllers(container);

        }

        private static void RegisterMvcControllers(IWindsorContainer container)
        {
            container.Register(
                Classes.
                    FromThisAssembly().
                    BasedOn<IController>().
                    If(c => c.Name.EndsWith("Controller")).
                    LifestyleTransient());
        }

        private static void RegisterWebApiControllers(IWindsorContainer container)
        {
            container.Register(Classes
                .FromThisAssembly()
                .BasedOn<IHttpController>()
                .ConfigureFor<ApiController>(c => c.PropertiesRequire(pi => false))
                .LifestyleTransient());
        }
    }
}