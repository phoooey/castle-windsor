using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using windsorTest.Plumbing;

namespace windsorTest.Installers
{
    public class InterceptorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                    .Where(type => type.IsPublic)
                    .WithService.DefaultInterfaces()
                    .Configure(c => c.LifestyleTransient()
                      .Interceptors<LoggingInterceptor,CacheInterceptor>()));
        }
    }
}