﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(windsorTest.Startup))]
namespace windsorTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
