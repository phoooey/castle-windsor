﻿using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using windsorTest.classes;
using windsorTest.Controllers;
using windsorTest.Installers;
using windsorTest.Plumbing;

namespace windsorTest
{
    public class MvcApplication : HttpApplication
    {
        private readonly IWindsorContainer _container = new WindsorContainer();

        protected void Application_Start()
        {

            _container.Register(Component.For<LoggingInterceptor>());
            _container.Register(Component.For<ICacheProvider>().ImplementedBy<WebCacheProvider>());
            _container.Register(Component.For<CacheInterceptor>());
            _container.Install(new ControllersInstaller(), new InterceptorInstaller());
          

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Services.Replace(
                typeof(IHttpControllerActivator),
                new WindsorCompositionRoot(_container));
            
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public override void Dispose()
        {
            _container.Dispose();
            base.Dispose();
        }
    }
}
