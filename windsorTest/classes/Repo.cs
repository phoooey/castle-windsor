﻿namespace windsorTest.classes
{
    public class Repo : IRepo
    {
        public virtual string GetMessage(Details details)
        {
            return "Hello " + details.Name;
        }
    }
}