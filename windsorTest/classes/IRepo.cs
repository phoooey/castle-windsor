﻿namespace windsorTest.classes
{
    public interface IRepo
    {
        string GetMessage(Details details);
    }
}