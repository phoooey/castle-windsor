﻿namespace windsorTest.classes
{
    public interface IService
    {
        string GetMessage(Details details);
    }
}