﻿namespace windsorTest.classes
{
    public class Service : IService
    {
        private readonly IRepo _repo;

        public Service(IRepo _repo)
        {
            this._repo = _repo;
        }

        public string GetMessage(Details details)
        {
            return _repo.GetMessage(details);
        }
    }
}